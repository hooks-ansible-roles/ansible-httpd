Role Name
=========

Simple role to install Apache.

Requirements
------------

None, but the firewalld role is recommended.

Role Variables
--------------

apache_pkg: automatically detected from distribution.

listen_port: Port for Apache to listen on

apache_user: Apache user

apache_group: Apache group

server_admin: Server email

www_html_override: AllowOverride setting for /var/www/html directory.

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: web_servers
      roles:
         - { role: apache-httpd, www_html_override: All }

License
-------

BSD

Author Information
------------------

John Hooks